# Listing of the NRT-Flare-Predictor config file lines

## TOC

* [Return to README](./README.md)
* [RESTFULAPI](#markdown-header-restfulapi)
* [LOGGING](#markdown-header-logging)
* [RUNTIME](#markdown-header-runtime)
* [MODELS](#markdown-header-models)
* [MODEL_WEIGHTS](#markdown-header-model_weights)
* [MODEL_INPUT_SUBSET](#markdown-header-model_input_subset)

***

## RESTFULAPI

This set of values is for configuration of the RESTFul API connection. We must have each of the sub values within the  
__RESTFULAPI__  tag. If they are not present, the configuration will fail. They do not need to be in any particular
order though.

	[RESTFULAPI]
    host = localhost
    port = 8000
    user = testuser
    password = password

### host

	host = localhost

This is the hostname of the container or server that holds the RESTFul api application that is used to serve and store
all the data produced by and accessed by this process.

### user

	user = root

This is the username to use when posting to the above RESTFul api application. All the get requests are performed
without authentication.

### password

	password = root123

This is the password to use when connecting to the above RESTFul api application.

### port

	port = 3306

This is the port that is exposed on the host that the above RESTFul api application.

[Return to TOC](#markdown-header-toc)

***

## LOGGING

This set of values is for configuration of logging of the process. We must have each of the sub values within the  __
LOGGING__  tag. If they are not present, the configuration will fail. They do not need to be in any particular order
though.

	[LOGGING]
	log_path = /app/log
	log_file = nrt-flare-predictor.log
	log_file_size_bytes = 10485760
	log_backups = 5
	level = DEBUG

### log_path

	log_path = /app/log

This is the location within the container that the process will write the log file. This is provided so it can be set to
a location that is mapped outside the container to a location of the users choosing. Doing such mapping makes the log
file available outside the container.

### log_file

	log_file = nrt-flare-predictor.log

This provides the naming convention for the set of rolling log files. The most current log file will be named as the
provided file name, and the rollover files will be numbered 1 to the number of backup files specified.

### log_file_size_bytes

	log_file_size_bytes = 10485760

This provides how large the log file will be allowed to grow in bytes prior to it being rolled over to a backup file and
a new empty file is started for fresh logging.

### log_backups

	log_backups = 5

This provides how many backup log files to keep after rollover has taken place. Each time a new file is created, the old
files are rolled over up to this number of times before they are simply deleted.

### level

	level = DEBUG

This provides the level of logging to perform. The levels available are  __DEBUG__ ,  __INFO__ , or  __ERROR__ . **
Note**: These are case sensitive! In the  __DEBUG__  mode, the most amount of information is provided, including stack
traces of errors that occur. In the  __INFO__  mode, slightly less information is recoreded, like times of start and
completion of the scheduled processing task but stack traces of errors are not recorded. In the  __ERROR__  mode, the
least amount of information is recorded. In this, only the error information is recorded, this includes what method the
error was captured in and what information the error provided, but no stack trace.

[Return to TOC](#markdown-header-toc)

***

## RUNTIME

This set of values is for configuration of the runtime frequency of the various sub-processes within this process. We
must have each of the sub values within the  __RUNTIME__  tag. If they are not present, the configuration will fail.
They do not need to be in any particular order though.

	[RUNTIME]
	model_path=./models
	cadence_hours=0.25
    batch_size=20

### cadence_hours

	cadence_hours=0.25

This provides how frequently the prediction process will run in hours.

### model_path

	model_path=./models

This provides the location of the stored models that will be loaded and used by this process to perform prediction. They
are part of the repository, so there isn't really any reason to change this. It was added to the config file in case
things change in the furure.

### batch_size

    batch_size=20

When processing, connections to the data storage API utilizes asynchronous session calls. This limits the number of
concurrent items can be processed at the same time while waiting for API responses. This is to limit the number of
concurrent http API requests are outstanding at any point in time.

[Return to TOC](#markdown-header-toc)

***

## MODELS

This set of values is for configuration of the names of the files containing the models that are used by this process.
We must have each of the sub values within the  __MODELS__  tag. If they are not present, the configuration will fail.
They do not need to be in any particular order though. This shouldn't be changed unless the entire process has been
changed.

	[MODELS]
	model_1=DSDO_TRp1p2p3p4p5.pkl
	model_2=NRT_TRp2p3p4p5.pkl
	model_3=SOHO_TRp1p2p3_TE_NSDOp4p5.pkl

[Return to TOC](#markdown-header-toc)

***

## MODEL_WEIGHTS

This set of values is for configuration of the weights for each of the models that are used by this process when
producing the metamodel output. We must have each of the sub values within the  __MODEL_WEIGHTS__  tag. If they are not
present, the configuration will fail. They do not need to be in any particular order though. This shouldn't be changed
unless the entire process has been changed.

	[MODEL_WEIGHTS]
	model_1=0.61
	model_2=0.65
	model_3=0.34

[Return to TOC](#markdown-header-toc)

***

## MODEL_INPUT_SUBSET

This set of values is for configuration of the subset of the input paramters that a model will use if it is less than
the default features. We must have each of the sub values within the  __MODEL_INPUT_SUBSET__  tag. If they are not
present, the configuration will fail. They do not need to be in any particular order though. This shouldn't be changed
unless the entire process has been changed.

	[MODEL_INPUT_SUBSET]
	model_3=USFLUX

[Return to TOC](#markdown-header-toc)

***

[Return to README](./README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)