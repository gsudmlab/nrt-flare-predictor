# NRT-Flare-Prediction Test Build

If you wish to run this project independently on your local host for test purposes, you will need to setup several
things first.

1. The required libraries listed in the `requirements.txt` file, you should setup a conda environment with the required
   versions of the libraries by first:

       conda create -n flare-predictor scipy==1.5.2 pandas==1.1.0 scikit-learn==0.23.2 statsmodels==0.11.1 numpy==1.19.1 requests~=2.28.1 aiohttp~=3.8.3

   Then using the `flare-predictor` environment by

       conda activate flare-predictor

   and finally install `sktime` using:

       pip install sktime==0.4.1


2. You should now be able to run the project using:

       python3 NRT-Flare-Predictor.py

3. When done you should deactivate the environment:

       conda deactivate

***

## Runtime

1. To run this project, access to the RESTFul API that stores the data used by and reported by this process is
   required. In order to run the API see
   the [sep-prediction-restful-db](https://bitbucket.org/gsudmlab/sep-prediction-restful-db/) project for
   instructions on setup for and running the API.

2. This process also utilizes data produced by
   the [NRT-HARP-Data-Processing](https://bitbucket.org/gsudmlab/nrt-harp-data-processing/) project. See that project to
   produce the data accessed by this process.

***

[Return to README](./README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)