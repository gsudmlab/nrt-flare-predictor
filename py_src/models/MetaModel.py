"""
 * NRT-Flare-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import joblib
from pandas import DataFrame


class MetaModel:

    def __init__(self, models_info: {}):
        self._models = self.__load_models_from_info(models_info)

    def predict_proba(self, data: DataFrame):
        results = []
        meta_results = [0] * 2
        w_sum = 0
        for model_info in self._models:
            model = model_info[0]
            weight = model_info[1]
            w_sum += weight
            if len(model_info) == 3:
                result = model.predict_proba(data[model_info[2]])
                results.append(result[0].tolist())
            else:
                result = model.predict_proba(data)
                results.append(result[0].tolist())
            meta_results[0] += result[0][0] * weight
            meta_results[1] += result[0][1] * weight
        meta_results[0] = meta_results[0] / w_sum
        meta_results[1] = meta_results[1] / w_sum
        results.insert(0, meta_results)

        return results

    @staticmethod
    def __load_models_from_info(models_info: {}):
        loaded_models = []
        for key, value in models_info.items():
            path = value['path']
            weight: float = value['weight']
            model = joblib.load(path)
            if 'subset' in value.keys():
                subset = value['subset']
                loaded_models.append((model, weight, subset))
            else:
                loaded_models.append((model, weight))
        return loaded_models
