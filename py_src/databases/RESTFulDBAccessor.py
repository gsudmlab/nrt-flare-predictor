"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2022 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
import aiohttp
import requests
import traceback

from typing import List
from logging import Logger

from pandas import DataFrame
from datetime import datetime

from ..datatypes.HARPParams import HARPParams
from ..datatypes.HARPRecord import HARPRecord
from ..datatypes.FlarePrediction import FlarePrediction


class RESTFulDBAccessor:

    def __init__(self, base_uri: str, user: str, password: str, logger: Logger):
        self._base_uri = base_uri
        self._unprocessed_uri = self._base_uri + 'flarepred/unprocessed/'
        self._params_range_uri = self._base_uri + 'harpdata/parameters/'
        self._files_range_uri = self._base_uri + 'harpdata/files/'
        self._predictions_uri = self._base_uri + 'flarepred/predictions/'
        self._token_uri = self._base_uri + 'generate-auth-token/'
        self._logger = logger
        self._user = user
        self._pass = password
        self._session = None
        self._async_session = None

    def __get_session(self):
        if self._session is not None:
            return self._session

        try:
            self._session = requests.Session()
            self._session.auth = (self._user, self._pass)
            data = {"username": self._user, "password": self._pass}
            r = self._session.post(self._token_uri, json=data)
            if r.status_code == 200:
                token_json = json.loads(r.content)
                token = token_json['token']
                token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                self._session.headers.update(token_header)
                return self._session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__session Traceback: %s', traceback.format_exc())
        return None

    def __reset_session(self):
        try:
            if self._session is not None:
                self._session.close()
                self._session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    async def __get_async_session(self):
        if self._async_session is not None:
            if self._async_session.closed is False and not self._async_session.loop.is_closed():
                return self._async_session
            else:
                self._async_session = None

        try:
            self._logger.info('RESTFulDBAccessor.get__async_session initializing session.')
            auth = aiohttp.BasicAuth(self._user, self._pass)
            conn = aiohttp.TCPConnector(limit=50, force_close=True)
            self._async_session = aiohttp.ClientSession(auth=auth, connector=conn)
            data = {"username": self._user, "password": self._pass}
            r = await self._async_session.post(self._token_uri, json=data)
            async with r:
                if r.status == 200:
                    token_json = await r.json()
                    token = token_json['token']
                    token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                    self._async_session.headers.update(token_header)
                    return self._async_session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__async_session Traceback: %s', traceback.format_exc())
            self._async_session = None
        return None

    async def __reset_async_session(self):
        try:
            if self._async_session is not None:
                await self._async_session.close()
                self._async_session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_async_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    def get_unprocessed(self) -> List[HARPRecord]:

        try:
            session = self.__get_session()
            if session is None:
                return None

            r = session.get(self._unprocessed_uri)

            if r.status_code == 200:
                results = []
                for obj in r.json():
                    harp_num = int(obj['harpnumber'])
                    obs_start_str = obj['obsstart']
                    obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')

                    harp_rec = HARPRecord(harp_num, obs_start)
                    results.append(harp_rec)
                return results
            else:
                self._logger.error('RESTFulDBAccessor.get_unprocessed returned unexpected status: %s',
                                   str(r.status_code))
                return None

        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_unprocessed Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_unprocessed Traceback: %s', traceback.format_exc())
            return None

    async def get_params_for_harp_between_async(self, harp_number: int, start_time: datetime,
                                                end_time: datetime) -> HARPParams:
        """
            Method gets the parameter values (ObsStart, HEAD_USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LAT_MIN,
            LON_MIN, LAT_MAX, LON_MAX, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS) for the specified HARP at each time step between
            the input times. They are placed into a dataframe with the index set to the ObsStart datetime. Then this is
            wrapped by an object to contain it and the harp number.

            :param harp_number: The harp number to select from the database
            :param start_time: The start time for values to select from the database, all values are from a time after this
            :param end_time: The last time for values to select from the database
            :return: HARPParams with DataFrame with the values in it, or None if nothing was found
        """

        header = ['ObsStart', 'USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'LAT_MIN',
                  'LON_MIN', 'LAT_MAX', 'LON_MAX', 'CRVAL1', 'CRVAL2', 'CRLN_OBS', 'CRLT_OBS']
        answer = []

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            qury_start = start_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury_end = end_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["starttime={}".format(qury_start), "endtime={}".format(qury_end),
                    "harp_number={}".format(harp_number)]

            complete_url = "{}?{}".format(self._params_range_uri, '&'.join(qury))

            async with session.get(complete_url) as response:
                result_json = await response.json()

                for step in result_json:
                    obs_start_str = step['obs_start']
                    obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                    USFLUX = float(step['head_usflux'])
                    TOTUSJZ = float(step['head_totusjz'])
                    TOTUSJH = float(step['head_totusjh'])
                    ABSNJZH = float(step['head_absnjzh'])
                    SAVNCPP = float(step['head_savncpp'])
                    TOTPOT = float(step['head_totpot'])
                    LAT_MIN = float(step['lat_min'])
                    LON_MIN = float(step['lon_min'])
                    LAT_MAX = float(step['lat_max'])
                    LON_MAX = float(step['lon_max'])
                    CRVAL1 = float(step['crval1'])
                    CRVAL2 = float(step['crval2'])
                    CRLN_OBS = float(step['crln_obs'])
                    CRLT_OBS = float(step['crlt_obs'])
                    answer.append([obs_start, USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LAT_MIN,
                                   LON_MIN, LAT_MAX, LON_MAX, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS])

        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_params_for_harp_between Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_params_for_harp_between Traceback: %s', traceback.format_exc())
            return None

        if len(answer) > 0:
            df = DataFrame(answer, columns=header)
            df.set_index('ObsStart')
            df.sort_values(by=['ObsStart'], ascending=False, inplace=True)
            param_obj = HARPParams(harp_number, df)
            return param_obj
        else:
            print("None")
            return None

    async def get_times_for_available_files_in_harp_between_async(self, harp_number: int, start_time: datetime,
                                                                  end_time: datetime) -> DataFrame:
        """
        Method gets the descriptor of files available for the specified HARP at each time step between the input times.
        They will be placed into a dataframe with the index set to the ObsStart datetime.

        :param harp_number: The harp number to select from the database
        :param start_time: The start time for values to select from the database, all values are from a time after this
        :param end_time:  The last time for values to select from the database
        :return:  DataFrame with the values in it, or None if nothing was found
        """
        header = ['ObsStart']
        answer = []

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            qury_start = start_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury_end = end_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["starttime={}".format(qury_start), "endtime={}".format(qury_end),
                    "harp_number={}".format(harp_number)]

            complete_url = "{}?{}".format(self._files_range_uri, '&'.join(qury))

            async with session.get(complete_url) as response:
                if response.status == 200:
                    result_json = await response.json()

                    for step in result_json:
                        obs_start_str = step['obsstart']
                        obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                        bit_ok = step['bitmapdownloaded']
                        conf_ok = step['confdownloaded']
                        mag_ok = step['magnetogramdownloaded']
                        if bit_ok == 1 and conf_ok == 1 and mag_ok == 1:
                            answer.append([obs_start])

        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_times_for_available_files_in_harp_between Failed with: %s',
                               str(e))
            self._logger.debug('RESTFulDBAccessor.get_times_for_available_files_in_harp_between Traceback: %s',
                               traceback.format_exc())
            return None

        if len(answer) > 0:
            df = DataFrame(answer, columns=header)
            df.set_index('ObsStart')
            df.sort_values(by=['ObsStart'], ascending=False, inplace=True)
            return df
        else:
            return None

    async def save_flare_prediction_async(self, flare_prediction: FlarePrediction):
        """
        Method inserts a flare prediction report into the flares table. If the report already exists
        then nothing is updated.

        :param flare_prediction: The data to insert into the table.
        :return: True if successful, false or throws an exception if not.
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return False

            pred_time = flare_prediction.ObsStart.strftime('%Y-%m-%dT%H:%M:%S')
            content_dict = {'harp_number': flare_prediction.HarpNumber, 'obsstart': pred_time,
                            'meta_flare_prob': flare_prediction.meta_flare_prob,
                            'meta_non_flare_prob': flare_prediction.meta_non_prob,
                            'mod1_flare_prob': flare_prediction.mod1_flare_prob,
                            'mod1_non_flare_prob': flare_prediction.mod1_non_prob,
                            'mod2_flare_prob': flare_prediction.mod2_flare_prob,
                            'mod2_non_flare_prob': flare_prediction.mod2_non_prob,
                            'mod3_flare_prob': flare_prediction.mod3_flare_prob,
                            'mod3_non_flare_prob': flare_prediction.mod3_non_prob,
                            'LAT_MIN': flare_prediction.LAT_MIN,
                            'LON_MIN': flare_prediction.LON_MIN,
                            'LAT_MAX': flare_prediction.LAT_MAX,
                            'LON_MAX': flare_prediction.LON_MAX,
                            'CRVAL1': flare_prediction.CRVAL1,
                            'CRVAL2': flare_prediction.CRVAL2,
                            'CRLN_OBS': flare_prediction.CRLN_OBS,
                            'CRLT_OBS': flare_prediction.CRLT_OBS}

            async with session.post(self._predictions_uri, json=content_dict) as r:
                if r.status == 201:
                    return True
                else:
                    return False
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.save_flare_prediction Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_flare_prediction Traceback: %s', traceback.format_exc())
            return False
